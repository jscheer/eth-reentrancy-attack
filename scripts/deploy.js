// Deployment script for the ReentrancyContract.
//
// test with: npx hardhat run scripts/deploy.js
//
const hre = require("hardhat");

async function main() {

  const [deployer] = await ethers.getSigners();

  console.log("Deploying contracts with the account:", deployer.address);
  console.log("ETH balance:", (await deployer.getBalance()).toString());

  const Contract = await ethers.getContractFactory("ReentrancyContract");
  const contract = await Contract.deploy();

  await contract.deployed();
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });


