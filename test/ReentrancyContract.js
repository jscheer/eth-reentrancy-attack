// Tests for ReentrancyContract.sol
//
// run tests with: npx hardhat test 
//

const {
  time,
  loadFixture,
} = require("@nomicfoundation/hardhat-network-helpers");
const { anyValue } = require("@nomicfoundation/hardhat-chai-matchers/withArgs");
const { expect } = require("chai");

describe("ReentrancyContract", function () {
  
  // Deployment fixtures
  async function deployFixture() {
    
    const [owner, otherAccount1, otherAccount2] = await ethers.getSigners();
    const Contract = await ethers.getContractFactory("ReentrancyContract");

    const contract = await Contract.deploy();
    await contract.deployed();

    return { contract, owner, otherAccount1, otherAccount2 };
  }

  /**
   * Test ReentrancyContract.deposit() function
   */
  describe("deposit", function () {
    it("Should deposit ETH", async function () {
      const { contract, owner, otherAccount1, otherAccount2 } = await loadFixture(deployFixture);

      // TODO: implement

    });

  }); // end of deposit

  /**
   * Test ReentrancyContract.withdraw() function
   */
  describe("withdraw", function () {
    it("Should withdraw EHT", async function () {
      const { contract, owner, otherAccount1, otherAccount2 } = await loadFixture(deployFixture);

      // TODO: implement

    });
  }); // end of withdraw

});   // end of ReentrancyContract
